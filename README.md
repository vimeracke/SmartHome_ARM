人工智能云家居
==
## 主体功能

![智能终端](http://vimeracke.oss-cn-shenzhen.aliyuncs.com/1.jpg "智能终端") ![智能终端](http://vimeracke.oss-cn-shenzhen.aliyuncs.com/2.jpg "智能终端")

---
## 项目分工

- __UI设计__
    + ARM端
        - 音视频播放
        - 家庭数据汇总
        - 检测到人就拍照发到微信
    * 手机端
        - 远程控制
        - 远程监控
- __数据库__

    + 录入进出时间
    + 家庭数据汇总

- __安防设计__
    - 门禁安防
    + 烟雾报警

- __监控设计__
    - 声控
    - 视频采集

- __网关设计__
    - 网络层通信

- __云端通信__
    - 服务器与各个终端进行交互

- __硬件电路__
    - 包装开发板
    - 设计布线
    - 设计模型外观

---
# ISSUE
## __UI功能__
### 阿里云API
 1. 天气预报
 2. 厨房菜谱
 3. 快递查询
 4. 短信服务
 5. 公交地铁查询
 6. 地图（可选)
 7. PM2.5
 8. 电子相册
![阿里云](http://vimeracke.oss-cn-shenzhen.aliyuncs.com/2018-01-17%2013-42-59%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE.png)
![阿里云](http://vimeracke.oss-cn-shenzhen.aliyuncs.com/2018-01-17%2013-43-24%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE.png)
![阿里云](http://vimeracke.oss-cn-shenzhen.aliyuncs.com/2018-01-17%2013-44-14%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE.png)
### 家居数据汇总
1. 各个房间信息

## 移植linaro ubuntu
---
## 项目成员

- **韩懿恺** git branch
- **吴楷锋** git branch
- **倪楷寻** git branch
- **黄瑞琪** git branch
- **朱泽榕** git branch

# 代码框架


```
├──STM32
    ├── src                         #源程序
    └── include                     #头文件
    


    
├──ARM
    ├── 3rd_party
    │   └── cJSON
    ├── CMakeLists.txt
    ├── docker
    │   ├── Dockerfile
    │   └── README.md
    ├── include
    │   └── api
    ├── Jenkinsfile
    ├── README.md
    ├── src
    │   ├── api
    │   ├── mysql
    │   ├── python
    │   └── video
    └── tensorflow
        └── object.py

```




**[模块购买地址](https://telesky.tmall.com/?ali_refid=a3_430583_1006:1109983619:N:telesky:febc3cccfa179117951bded6ec618d51&ali_trackid=1_febc3cccfa179117951bded6ec618d51&spm=a230r.1.14.4)**<br>

**[Gitlab地址](https://gitlab.com/vimeracke/SmartHome_ARM)**<br>

> 当你的才华撑不起你的野心时<br>
> 那就安静下来学习吧<br>

***Written in 2018***
