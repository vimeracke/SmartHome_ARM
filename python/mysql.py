#!/usr/bin/env python
# coding=utf-8

import pymysql

def test_add:
# 打开数据库连接
    conn = pymysql.connect(
        host = "120.79.173.163",
        user = "root",
        password = "123",
        database = "mysql",
        charset = 'utf8',
        cursorclass = pymysql.cursors.DictCursor)

    cursor = conn.cursor()
    # 使用 execute()  方法执行 SQL 查询
    cursor.execute("SELECT VERSION()")

    # 使用 fetchone() 方法获取单条数据.
    data = cursor.fetchone()

    print("Database version : %s " % data)

    # 关闭数据库连接
    conn.close()
