#ifndef WEATHER_H_
#define WEATHER_H_

#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <assert.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <string.h>
#include <strings.h>

#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "3rd_party/cJSON/cJSON.h"
using namespace std;

class Weather
{
public:
	explicit Weather();
	~Weather();

public:
	void weather(int code);

private:
	struct hostent;


private:
	void remote_info(struct hostent *he);
	void http_request(char*buf, int size, char *phone_code);
	void show_weather_info(char *json);


};

#endif	//WEATHER_H_
