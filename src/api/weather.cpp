// Copyright [2018] <acke>
#include <include/api/weather.h>
Weather::Weather()
{

}

void remote_info(struct hostent *he)
{
    // 判错
    assert(he);

    cout <<"主机的官方名称" << he->h_name << endl;

    int i;
    for (i = 0; he->h_aliases[i] != NULL; i++) {
        printf("别名[%d]：%s\n", i+1, he->h_aliases[i]);
    }

    printf("IP地址长度：%d\n", he->h_length);

    for (i=0; he->h_addr_list[i] !=NULL; i++) {
        printf("IP地址[%d]：%s\n", i+1,
               inet_ntoa(*((struct in_addr **)he->h_addr_list)[i]));
    }
}


void http_request(char *buf, int size, char *phone_code)
{
    assert(buf);
    assert(phone_code);

    memset(buf, 0, size);
    snprintf(buf, size, "GET /phone-post-code-weeather?"
             "phone_code=%s "
             "HTTP/1.1\r\n"
             "Host:ali-weather.showapi.com\r\n"
             "Authorization:APPCODE d487d937315848af80710a06f4592fee\r\n\r\n",
             phone_code);
}

// 解析并显示查询后得到的数据
void show_weather_info(char *json)
{
    cJSON *root     = cJSON_Parse(json);
    cJSON *body     = cJSON_GetObjectItem(root, "showapi_res_body");

    if (cJSON_GetObjectItem(body, "ret_code")->valueint == -1) {
        printf("%s\n", cJSON_GetObjectItem(body, "remark")->valuestring);
        return;
    }

    cJSON *now      = cJSON_GetObjectItem(body, "now");
    cJSON *cityInfo = cJSON_GetObjectItem(body, "cityInfo");
    cJSON *today    = cJSON_GetObjectItem(body, "f1");
    cJSON *tomorrow = cJSON_GetObjectItem(body, "f2");
    cJSON *day_3rd  = cJSON_GetObjectItem(body, "f3");

    char *country = cJSON_GetObjectItem(cityInfo, "c9")->valuestring;
    char *province = cJSON_GetObjectItem(cityInfo, "c7")->valuestring;
    char *city    = cJSON_GetObjectItem(cityInfo, "c5")->valuestring;

    bool zhixiashi = !strcmp(city, province);
    printf("城市：%s·%s%s%s\n\n", country, province, zhixiashi ? "" : "·", zhixiashi ? "" : city);

    printf("现在天气：%s\n",    cJSON_GetObjectItem(now, "weather")->valuestring);
    printf("现在气温：%s°C\n\n",cJSON_GetObjectItem(now, "temperature")->valuestring);

    printf("明天天气：%s\n",     cJSON_GetObjectItem(tomorrow, "day_weather")->valuestring);
    printf("日间气温：%s°C\n",   cJSON_GetObjectItem(tomorrow, "day_air_temperature")->valuestring);
    printf("夜间气温：%s°C\n\n", cJSON_GetObjectItem(tomorrow, "night_air_temperature")->valuestring);

    printf("后天天气：%s\n",     cJSON_GetObjectItem(day_3rd, "day_weather")->valuestring);
    printf("日间气温：%s°C\n",   cJSON_GetObjectItem(day_3rd, "day_air_temperature")->valuestring);
    printf("夜间气温：%s°C\n\n", cJSON_GetObjectItem(day_3rd, "night_air_temperature")->valuestring);
}

void weather(int code)
{

}

int main(int argc, char **argv)
{
    cout <<"input phone code" <<endl;

    char *phone_code = new char[5];
    cin >> phone_code;

    phone_code = strtok(phone_code, "\n");
    // 调试控制台接口域名
    char *host = "ali-weather.showapi.com";
    // 解析域名成IP
    struct hostent *he = gethostbyname(host);
    if (he == NULL) {
        errno = h_errno;
        cerr <<"gethostbyname() failed" <<endl;
        exit(0);
    }
    // TODO(acke)
    struct in_addr **addr_list = (struct in_addr **)(he->h_addr_list);
    // socket 接口
    int fd = socket(AF_INET, SOCK_STREAM, 0);

    struct sockaddr_in srvaddr;
    socklen_t len = sizeof(srvaddr);
    memset(&srvaddr, 0, len);

    srvaddr.sin_family = AF_INET;
    srvaddr.sin_port   = htons(80);
    srvaddr.sin_addr   = *addr_list[0];

    if (connect(fd, (struct sockaddr *)&srvaddr, len) == -1) {
        cerr <<"connect() failed" <<endl;
        exit(0);
    }

    char *sndbuf = new char[1000];
    // 调用函数发起http请求
    http_request(sndbuf, 1000, phone_code);
    delete [] phone_code;

    int n = send(fd, sndbuf, strlen(sndbuf), 0);
    if (n == -1) {
        cerr <<"send() failed" <<endl;
        exit(0);
    }

    delete [] sndbuf;

    char *recvbuf = new char[4096];
    int m = 0;

    while (1) {
        n = recv(fd, recvbuf+m, 2000, 0);
        if (n == 0)
            break;
        if (n == -1) {
            cerr <<"recv() failed" <<endl;
            exit(0);
        }
        m += n;

        if (strstr(recvbuf, "}}") || strstr(recvbuf, "400 Bad Request"))
            break;

        if (strstr(recvbuf, "Quota Exhausted")) {
            cout <<"API查询次数已超，请续费" <<endl;
            exit(0);
        }
    }

    show_weather_info(strstr(recvbuf, "{"));
    delete [] recvbuf;

    return 0;
}
