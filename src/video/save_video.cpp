/*************************************************************************
	> File Name: save_video.cpp
	> Author: acke
	> Mail: vimer757216574@gmail.com
	> Created Time: 2018年02月02日 星期五 10时12分56秒
 ************************************************************************/

#include<iostream>
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>


using namespace std;
int main()
{
	//输出文件路径
	string videopath = "./save.avi";
	cv::VideoCapture capture(0);
	cv::VideoWriter outputVideo;
	int codec = CV_FOURCC('M', 'J', 'P', 'G');
	cv::Size size = cv::Size((int)capture.get(CV_CAP_PROP_FRAME_WIDTH),
                          	(int)capture.get(CV_CAP_PROP_FRAME_HEIGHT));
	outputVideo.open(videopath, codec, 40.0, size ,true);
	//打开错误判断
	if(!outputVideo.isOpened()){
		cout<<"fail to open"<<endl;
		exit(0);
	}	

	cv::Mat frameImage;
	int count = 0;
	while(true){
		capture >> frameImage;
		//写入错误判断
		if(frameImage.empty())
			break;

		++count;
		cv::imshow("video", frameImage);
		outputVideo << frameImage;
		if(char(cv::waitKey(1)) == 'q')
			break;
	}
}
