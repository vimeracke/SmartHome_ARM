//
// Created by thinkjoy on 12/29/17.
//
#include <vector>
#include <sqlite3.h>
#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <stdlib.h>
#include <cctype>
#include <cstring>
struct MemberInfo {
    int id;
    long add_time;
    int mode;
    int temprature;
    int speed;
    int dir;
  };
int Creat_table(sqlite3 *member_db)
{
    sqlite3_open("member.db", &member_db);

    char *errmsg = nullptr;
    const char *sql = "create table if not exists member_table(" \
                      "id INTEGER primary key NOT NULL,"\
                      "add_time INTEGER,"               \
                      "mode INTEGER,"                   \
                      "temprature INTEGER,"             \
                      "speed INTEGER,"                  \
                      "dir INTEGER );";

    int ret = sqlite3_exec(member_db, sql, nullptr, nullptr, &errmsg);
    sqlite3_close(member_db);
    
    if(ret != SQLITE_OK)
    {
        perror("sqlite3_exec\n");
        std::cout<<errmsg<<std::endl;
        delete(errmsg);
        return -1;
    }
    delete(errmsg);

    return 0;
}

int save_data(struct MemberInfo MemberInfo, sqlite3 *member_db)
{
    sqlite3_open("member.db", &member_db);

    char *errmsg = nullptr;
    char *save = new char[256];

    sprintf(save, "INSERT INTO member_table VALUES        \
           (%d, %ld , %d , %d , %d , %d);",               \
            MemberInfo.id, MemberInfo.add_time,           \
            MemberInfo.mode, MemberInfo.temprature,       \
            MemberInfo.speed, MemberInfo.dir);            \

    int ret = sqlite3_exec(member_db, save, nullptr, nullptr, &errmsg);

    delete(save);
    sqlite3_close(member_db);
    if(ret != SQLITE_OK)
    {
        perror("sqlite3_exec\n");
        std::cout<<errmsg;
        delete(errmsg);
        return -1;
    }
    delete(errmsg);

    return 0;
}

std::vector<MemberInfo>load_data(sqlite3 *member_db)
{
    sqlite3_open("member.db", &member_db);
    char *errmsg = nullptr;
    const char *load = new char[256];
    char **result;

    int row=0;
    int column = 0;
    std::vector<MemberInfo> vector_load(30);

    load="select * from member_table";
    int ret = sqlite3_get_table(member_db,load,&result,&row,&column,&errmsg);

    sqlite3_close(member_db);
    if(ret != SQLITE_OK)
    {
        perror("sqlite3_get_table\n");
        std::cout<<errmsg;

    }

    int i = 1;
    int j = 0;

    // for(std::vector<MemberInfo>::iterator it = vector_load.begin(); it != vector_load.end(); it++, i++) {
    //
    //     it->id = atoi(result[(6 * i)]), \
    //     it->add_time = atoi(result[(6 * i) + 1]), \
    //     it->mode = atoi(result[(6 * i) + 2]), \
    //     it->temprature = atoi(result[(6 * i) + 3]), \
    //     it->speed = atoi(result[(6 * i) + 4]), \
    //     it->dir = atoi(result[(6 * i) + 5]);
    // }

    for( i = 1; i < row ; i++ )
    {
        vector_load[i-1].id = atoi(result[(6 * i)]), \
        vector_load[i-1].add_time = atoi(result[(6 * i) + 1]), \
        vector_load[i-1].mode = atoi(result[(6 * i) + 2]), \
        vector_load[i-1].temprature = atoi(result[(6 * i) + 3]), \
        vector_load[i-1].speed = atoi(result[(6 * i) + 4]), \
        vector_load[i-1].dir = atoi(result[(6 * i) + 5]);

        // std::cout<<vector_load[i-1].id<<std::endl;
        // std::cout<<vector_load[i-1].add_time<<std::endl;
        // std::cout<<vector_load[i-1].mode<<std::endl;
        // std::cout<<vector_load[i-1].temprature<<std::endl;
        // std::cout<<vector_load[i-1].speed<<std::endl;
        // std::cout<<vector_load[i-1].dir<<std::endl;

    }

    sqlite3_free_table(result);//释放result的内存空间
    delete(errmsg);

    return vector_load;
}

int update_data(struct MemberInfo MemberInfo, sqlite3 *member_db, int update_id)
{
    sqlite3_open("member.db", &member_db);
    char *errmsg = nullptr;
    char *update = new char[256];
    sprintf(update, "UPDATE member_table SET add_time =%ld,\
            mode =%d,temprature =%d,speed =%d,dir =%d      \
            where id =%d; select * from member_table;",    \
            MemberInfo.add_time, MemberInfo.mode,          \
            MemberInfo.temprature,MemberInfo.speed,        \
            MemberInfo.dir, MemberInfo.id);

    int ret = sqlite3_exec(member_db, update, nullptr, nullptr, &errmsg);
    delete(update);
    sqlite3_close(member_db);
    if(ret != SQLITE_OK)
    {
        perror("sqlite3_exec\n");
        std::cout<<errmsg<<std::endl;
        delete(errmsg);
        return -1;
    }
    delete(errmsg);

    return 0;
}

int main(void)
{
   sqlite3 *member_db;

   int ret = Creat_table(member_db);
   std::vector<MemberInfo> vector_load(30);

   struct MemberInfo mem{5,20080322,1,1,1,1};
//   save_data(mem, member_db);
   // update_data(date, db, update_id);

   vector_load = load_data(member_db);
   for(std::vector<MemberInfo>::iterator it = vector_load.begin(); it != vector_load.end(); it++)
   {
      if((*it).id!=0){
          std::cout<<it->id<<std::endl;
          std::cout<<it->add_time<<std::endl;
          std::cout<<it->mode<<std::endl;
          std::cout<<it->temprature<<std::endl;
          std::cout<<it->speed<<std::endl;
          std::cout<<it->dir<<std::endl<<std::endl;
      }

   }

   // std::cout<<load.add_time<<std::endl;

   // std::cout<<load<<std::endl;
   return 0;
}
