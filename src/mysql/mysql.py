#!/usr/bin/env python
# coding=utf-8

import pymysql
def mysql():
# 建立数据库连接
    conn = pymysql.connect(
        host="120.79.173.163",
        user="root",
        password="123",
        database="db_test",
        charset='utf8',
        cursorclass=pymysql.cursors.DictCursor)
    # 创建数据库
    # sql = '''
    # CREATE DATABASE testDB;
    # '''

    # 创建游标
    cursor = conn.cursor()
    # 创建数据库(如果不存在)
    # cursor.execute("""create database if not exists db_test""")
    # conn.commit()
    # 使用 execute()  方法执行 SQL 查询
    cursor.execute("SELECT VERSION()")
    # 使用 fetchone() 方法获取单条数据
    data = cursor.fetchone()
    print("Database version : %s " % data)

    sql = """CREATE TABLE if not exists test (
         FIRST_NAME  CHAR(20) NOT NULL,
         LAST_NAME  CHAR(20),
         AGE INT,  
         SEX CHAR(1),
         INCOME FLOAT )"""
    try:
        # 执行sql语句
        cursor.execute(sql)
        print("create succeed\n")
        # 提交到数据库执行
    except:
        # 如果发生错误则回滚
        conn.rollback()
        print("create failed\n")

    # 关闭数据库连接

    conn.close()


if __name__ == '__main__' :
    mysql()
