/*************************************************************************
	> File Name: cpp.cpp
	> Author: acke
	> Mail: vimer757216574@gmail.com
	> Created Time: 2018年02月28日 星期三 14时58分18秒
 ************************************************************************/
#include <Python.h>  
int main(int argc, char** argv)  
{  
    Py_Initialize();  
 
    if ( !Py_IsInitialized() ) {  
        return -1;  
    }  
    PyRun_SimpleString("import sys");  
    PyRun_SimpleString("print '---import sys---'");   
    PyRun_SimpleString("sys.path.append('./')");  
    PyObject *pName,*pModule,*pDict,*pFunc,*pArgs;  
  
    // 载入名为pytest的脚本  
    pName = PyString_FromString("mysql");  
    pModule = PyImport_Import(pName);  
    if ( !pModule ) {  
        printf("can't find mysql_add.py");  
        getchar();  
        return -1;  
    }  
    pDict = PyModule_GetDict(pModule);  
    if ( !pDict ) {  
        return -1;  
    }  
  
    // 找出函数名为add的函数  
    pFunc = PyDict_GetItemString(pDict, "mysql");  
    if ( !pFunc || !PyCallable_Check(pFunc) ) {  
        printf("can't find function [mysql]");  
        getchar();  
        return -1;  
    }    
    pArgs = PyTuple_New(0);  
    PyObject_CallObject(pFunc,pArgs);  
    // 关闭Python  
    Py_Finalize();  
    return 0;  
}   
